export interface IFoodItem  {
  name_eng: string;
  name_est: string;
  price: string;
  providers: Array<any>;
}
