import { Component } from '@angular/core';
import { IFoodItem } from './interfaces/IFoodItem';
import { DataService } from './services/DataService';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'food-app-angular';
  private foodItems: IFoodItem[] = [];
  private fObject: object;

  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    this.dataService
      .getAll<IFoodItem>()
      .subscribe((data) => this.foodItems = data['data'],
      error => () => {
        console.log('error', 'Damn', 'Something went wrong...');
      },
      () => {
        console.log(this.foodItems)
        console.log('success', 'Complete', 'Getting all values complete');
    });

  }
}
