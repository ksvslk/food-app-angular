import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public apiUrl = 'https://api.fuud.ituk.ee/daily';
}
